import requests
from bs4 import BeautifulSoup

#response = requests.get('http://example.com')
#print(response.status_code)
#print(response.content)
#print(response.text)

#response = requests.get('https://api.github.com/events')
#js = response.json()
#print([elem['id'] for elem in js])

#response = requests.get('https://bash.im')
#print(response.status_code)
#soup = BeautifulSoup(response.text, 'html.parser')
#print(soup.title)
#quotes = soup.find_all('div', 'quote')
#
# print(quotes[1])
# print(len(quotes))

response = requests.get('https://stackoverflow.com/search?q=python')
# print(response.headers['content-type'])
print(response.status_code)
soup = BeautifulSoup(response.text, 'html.parser')
# print(soup.find_all('div', 'flush-left')[0])
# print(soup.find_all('div', id = '50943744'))
questions = soup.find_all('div', 'question-summary')
#print(questions[0])
# print(questions[0].find('a'))
for question in questions:
    if question.find('div', 'status answered'):
        # print(question.find('a'))
        link = requests.get("https://www.stackoverflow.com" + question.a.attrs.get('href'))
        soup2 = BeautifulSoup(link.text, 'html.parser')
        print(soup2)
        answers = soup2.find_all('div', 'post-text')

        print(answers[0])
        print('\n')

